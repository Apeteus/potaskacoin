const fs = require('fs');
const Utils = require('./lib/Utils');
const Blockchain = require('./lib/Blockchain');
const TransactionList = require('./lib/TransactionList');
const Transaction = require('./lib/Transaction');

const toBase64 = (str) => Buffer.from(str).toString('base64');
const fromBase64 = (str) => Buffer.from(str, 'base64').toString('utf8');

const output = (str) => console.log(str);
const error = (err) => {
  console.log(err);
  process.exit();
};

const initialize = () => {
  if (!fs.existsSync('./chain.json')) {
    output('Creating chain.json...');
    fs.writeFileSync('./chain.json', '[]');
  }
  if (!fs.existsSync('./pending.json')) {
    output('Creating pending.json...');
    fs.writeFileSync('./pending.json', '[]');
  }
}

const commands = {
  help() {
    output('Usage: potaskacoin <command> <arguments>');
    output('Commands:');
    output('  help                       - Shows this help message')
    output('  share                      - Prints blockchain and transactions in base64 format');
    output('  load chain <base64>        - loads blockchain ');
    output('  load transactions <base64> - loads transactions ');
    output('  init                       - initializes your wallet');
    output('  balance                    - check wallet balance');
    output('  give <amount> <to>         - moves <amount> from your wallet to <to>');
    output('  validate                   - checks if current blockchain is valid');
    output('  mine [<nonce>]             - starts mining a new block with all current pending transactions, optionally continue from');
    output('                               <nonce>');
    output('  multimine <add> <offset>   - can be used to mine on multiple processes, resulting in higher speeds');
    output('  [<nonce>]');
  },
  share() {
    const chain = fs.readFileSync('./chain.json', 'utf8');
    output('Blockchain base64:');
    output('-------------------');
    output(toBase64(chain));
    output('-------------------');
    const transactions = fs.readFileSync('./pending.json', 'utf8');
    output('Transactions base64:');
    output('-------------------');
    output(toBase64(transactions));
    output('-------------------');
  },
  load(args) {
    if (args.length < 2) {
      error('Not enough arguments');
    }
    let base64 = args[1];
    let filename = '';
    const decoded = fromBase64(base64);
    if (args[0] === 'chain') {
      output('Loading blockchain...');
      filename = './chain.json';
      //fs.writeFileSync(, decoded);
    } else if (args[0] === 'transactions') {
      output('Loading transactions...');
      filename = './pending.json';
      //const old = JSON.parse(fs.readFileSync(filename, 'utf8'));
      //fs.writeFileSync(filename, JSON.stringify(old.concat(JSON.parse(decoded))));
    } else {
      error('Unknown type');
    }
    fs.writeFileSync(filename, decoded);
    
  },
  init() {
    if (fs.existsSync('./config.json')) {
      error('Wallet already exists! Aborting...');
    }
    output('Creating your new wallet...');
    Utils.initConfig();
    output('Done');
  },
  balance() {
    const chain = Blockchain.loadBlockchain(fs.readFileSync('./chain.json', 'utf8'));
    const keys = Utils.initConfig();
    const address = keys.publicKey;
    let balance = 0;
    chain.blocks.forEach((b) => {
      if (typeof b.data === 'object') {
        b.data.transactions.forEach((t, i) => {
          if (b.data.isTransactionValid(t, i)) {
            if (t.to === address) {
              balance += t.amount;
            } else if (t.from === address) {
              balance -= t.amount;
            }
          }
        });
        console.log(b.miner === address);
        if (b.miner === address) {
          balance += 10;
        }
      }
    });
    output(address);
    output(`Balance: ${balance} pc`);
  },
  give(args) {
    if (args.length < 2) {
      error('Not enough arguments');
    }
    const keys = Utils.initConfig();
    const to = args.slice(1).join(' ').replace(/\\n/g, '\n');
    const t = new Transaction(new Date().getTime(), keys.publicKey, to, parseInt(args[0], 10));
    t.sign(keys.privateKey);
    const oldPending = JSON.parse(fs.readFileSync('./pending.json', 'utf8'));
    const newPending = JSON.stringify(oldPending.concat([t]), null, 2);
    fs.writeFileSync('./pending.json', newPending);
    output('Transaction added to pending transactions');
    output('You should share your transactions to other peers:');
    const transactions = fs.readFileSync('./pending.json', 'utf8');
    output('-------------------');
    output(toBase64(transactions));
    output('-------------------');
  },
  validate() {
    const chain = Blockchain.loadBlockchain(fs.readFileSync('./chain.json', 'utf8'));
    output(
      chain.isValid()
      ? 'Blockchain is valid'
      : 'Blockchain is not valid'
    );
  },
  mine(args) {
    const keys = Utils.initConfig();
    const chain = Blockchain.loadBlockchain(fs.readFileSync('./chain.json', 'utf8'), keys.publicKey);
    if (chain.blocks.length > 0) {
      if (chain.isValid()) {
        const temp = chain.newBlock();
        const list = new TransactionList(temp.index);
        const pending = JSON.parse(fs.readFileSync('./pending.json', 'utf8'));
        pending.forEach((t) => {
          list.addSigned(t.index, t.signature, t.from, t.to, t.amount);
        });
        if (list.transactions.length > 0) {
          output(`Starting at ${new Date().toLocaleString()}`);
          temp.data = list;
          temp.mineBlock(chain.difficulty, chain.miner, parseInt(args[0], 10) || 0, 1);
          chain.blocks.push(temp);
          //chain.addBlock(temp);
          console.log('Block mined!');
          fs.writeFileSync('./pending.json', '[]');
          console.log(chain);
          console.log(chain.isValid());
          if (chain.isValid()) {
            fs.writeFileSync('chain.json', chain.toJsonText());
          }
        } else {
          output('Can\'t mine an empty block');
        }
      } else {
        output('Invalid block chain');
      }
    } else {
      output('Empty block chain');
    }
  },
  multimine(args) {
    if (args.length < 2) {
      error('Not enough arguments');
    }
    const keys = Utils.initConfig();
    const chain = Blockchain.loadBlockchain(fs.readFileSync('./chain.json', 'utf8'), keys.publicKey);
    if (chain.blocks.length > 0) {
      if (chain.isValid()) {
        const temp = chain.newBlock();
        const list = new TransactionList(temp.index);
        const pending = JSON.parse(fs.readFileSync('./pending.json', 'utf8'));
        pending.forEach((t) => {
          list.addSigned(t.index, t.signature, t.from, t.to, t.amount);
        });
        if (list.transactions.length > 0) {
          output(`Starting at ${new Date().toLocaleString()}`);
          temp.data = list;
          temp.mineBlock(chain.difficulty, chain.miner, (parseInt(args[2], 10) || 0) - parseInt(args[1], 10), parseInt(args[0], 10), parseInt(args[1], 10));
          chain.blocks.push(temp);
          //chain.addBlock(temp);
          output(`Block mined!`);
          output(`${chain.isValid()}`)
          output(`Nonce: ${temp.nonce}`);
          output(`Hash: ${temp.hash}`);
        } else {
          output('Can\'t mine an empty block');
        }
      } else {
        output('Invalid block chain');
      }
    } else {
      output('Empty block chain');
    }
  }
};

const parseArgs = (args) => {
  let command = '';
  args.forEach((arg) => {
    if (Object.keys(commands).includes(arg) && command === '') {
      command = arg;
    }
  });

  if (command !== '') {
    commands[command](args.slice(3));
  } else {
    output('"potaskacoin help" to get help');
  }
};

initialize();
parseArgs(process.argv);
