const Block = require('./Block');
const TransactionList = require('./TransactionList');
const Transaction = require('./Transaction');

class Blockchain {
  constructor(miner) {
    this.difficulty = 7;
    this.blocks = [];
    this.miner = miner;
  }

  static loadBlockchain(text, miner) {
    const parsed = JSON.parse(text);
    const chain = new Blockchain(miner);
    chain.blocks = parsed;
    chain.blocks.forEach((block, i) => {
      if (typeof block.data === 'object') {
        chain.blocks[i].data = Object.assign(new TransactionList, block.data);
        block.data.transactions.forEach((t, j) => {
          chain.blocks[i].data.transactions[j] = Object.assign(new Transaction, t);
        })
      } 
    });
    return chain;
  }

  toJsonText() {
    return JSON.stringify(this.blocks, null, 2);
  }

  isFirstBlockValid() {
    if (this.blocks[0].index !== 0
    || this.blocks[0].previousHash !== undefined
    || this.blocks[0].hash === undefined
    || Block.calculateHash(this.blocks[0]) !== this.blocks[0].hash
    || !Block.isHashValid(this.blocks[0], this.difficulty)) {
      return false;
    }
    return true;
  }

  isBlockValid(block, prev) {
    if (prev.index + 1 !== block.index
    || block.prevHash !== prev.hash
    || Block.calculateHash(block) !== block.hash
    || !Block.isHashValid(block, this.difficulty)
    || block.transactions === undefined
    || block.transactions.length <= 0) {
      return false;
    }
    return true;
  }

  isValid() {
    if (!this.isFirstBlockValid()) {
      return false;
    }

    const signatures = [];
    for (let i = 1; i < this.blocks.length; i++) {
      if (!this.isBlockValid(this.blocks[i], this.blocks[i - 1])) {
        return false;
      }
      if (this.blocks[i].data.transactions !== undefined) {
        this.blocks[i].data.transactions.forEach((t) => {
          signatures.push(t.signature);
          if (!this.blocks[i].data.isValid()) {
            return false;
          }
        });
      }
    }
    const hasDups = (new Set(signatures)).size !== signatures.length;
    if (hasDups) {
      return false;
    }

    return true;
  }

  getLastBlock() {
    return this.blocks.slice(-1)[0];
  }

  newBlock(data) {
    const last = this.getLastBlock();
    return new Block(last.index + 1, data, last.hash);
  }

  addBlock(b) {
    if (b instanceof Block) {
      b.mineBlock(this.difficulty, this.miner, 0, 1);
      this.blocks.push(b);
    }
  }
}

module.exports = Blockchain;
