const Sign = require('./Sign');

class Transaction {
  constructor(index, from, to, amount, signature) {
    this.index = index;
    this.from = from;
    this.to = to;
    this.amount = amount;
    this.signature = signature || '';
  }

  text() {
    return `${this.from}${this.to}${this.amount}`;
  }

  sign(secretKey) {
    this.signature = Sign.sign(this.text(), secretKey);
  }

  verify() {
    return Sign.verify(this.text(), this.signature, this.from);
  }
}

module.exports = Transaction;
