const crypto = require('crypto');
const sha256 = str => crypto.createHash('sha256').update(str).digest('hex')
const repeat = (count, str) => new Array(count + 1).join(str);

class Block {
  constructor(index, data, prevHash) {
    this.index = index;
    this.timestamp = new Date().getTime();
    this.data = data;
    this.prevHash = prevHash;
    this.nonce = 0;
    this.hash = Block.calculateHash(this);
    this.miner = '';
  }

  static calculateHash(b, data) {
    return sha256(`${b.index}${data || JSON.stringify(b.data)}${b.prevHash}${b.nonce}${b.miner}`);
  }

  static isHashValid(b, difficulty) {
    return this.calculateHash(b).slice(0, difficulty) === repeat(difficulty, '0');
  }

  mineBlock(difficulty, miner, offset, addition, debugNum) {
    this.nonce = offset;

    this.miner = miner;
    this.startTime = new Date().getTime();
    const data = JSON.stringify(this.data);
    while (!Block.isHashValid(this, difficulty)) {
      this.nonce += addition;
      this.hash = Block.calculateHash(this, data);
      if (this.nonce % 1000000 === (debugNum || 0)) {
        let elapsed = (new Date().getTime() - this.startTime) / 1000;
        let hashrate = (this.nonce - offset) / (elapsed);
        console.log(`Nonce: ${this.nonce}, hashrate: ${Math.round(hashrate)/1000}`);
      }
    }
    let elapsed = (new Date().getTime() - this.startTime) / 1000;
    let hashrate = (this.nonce - offset) / (elapsed);
    console.log(`Average ${Math.round(hashrate)/1000} khashes/s`);
    console.log(`Time passed: ${elapsed} s`);
  }
}

module.exports = Block;
