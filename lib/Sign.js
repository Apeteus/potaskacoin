const crypto = require('crypto');
const fs = require('fs');
const NodeRSA = require('node-rsa');

class Sign {
  static generateKeys() {
    const key = new NodeRSA({b: 1024});

    return {
      public: key.exportKey('pkcs1-public-pem'),
      private: key.exportKey('pkcs1-private-pem')
    }
  }

  static sign(text, secret) {
    const key = new NodeRSA(secret);
    return key.sign(text, 'base64');
  }

  static verify(text, signature, pub) {
    const key = new NodeRSA(pub);
    return key.verify(text, signature, undefined, 'base64')
  }
}

module.exports = Sign;
