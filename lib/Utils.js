const Sign = require('./Sign');
const fs = require('fs');

class Utils {
  static initConfig() {
    if (!fs.existsSync('./config.json')) {
      const keys = Sign.generateKeys();
      fs.writeFileSync('./config.json', JSON.stringify(
        {
          publicKey: keys.public,
          privateKey: keys.private
        },
      null, 2));
    }
    let pub;
    let priv;
    try {
      const config = JSON.parse(fs.readFileSync('./config.json', 'utf8'));
      pub = config.publicKey;
      priv = config.privateKey;
    } catch (e) {
      console.log('Invalid config.json');
      process.exit();
    }
    
    return { publicKey: pub, privateKey: priv };
  }
}

module.exports = Utils;
