const Transaction = require('./Transaction');

class TransactionList {
  constructor(blockIndex) {
    this.block = blockIndex;
    this.transactions = [];
  }

  getIndex(i) {
    return i;
  }

  add(secretKey, from, to, amount) {
    const wasValid = this.isValid();
    const t = new Transaction(new Date().getTime(), from, to, amount);
    try {
      t.sign(secretKey);
    } catch (e) {
      return false;
    }
    this.transactions.push(t);
    if (this.isValid() !== wasValid) {
      this.transactions.pop();
      return false;
    }
    return true;
  }

  addSigned(index, signature, from, to, amount) {
    const wasValid = this.isValid();
    const t = new Transaction(index, from, to, amount, signature);
    this.transactions.push(t);
    if (this.isValid() !== wasValid) {
      this.transactions.pop();
      return false;
    }
    return true;
  }

  isTransactionValid(t, i) {
    if (!t.verify()) {
      return false;
    }
    return true;
  }

  isValid() {
    for (let i = 0; i < this.transactions.length; i++) {
      if (!this.isTransactionValid(this.transactions[i], i)) {
        return false;
      }
    }
    return true;
  }
}

module.exports = TransactionList;
