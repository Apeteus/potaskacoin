# PotaskaCoin

A cryptocurrency made from scratch in Node.JS and a CLI (command line interface) to use it.

## Peer to peer

Potaskacoin, at least currently, does not have a peer to peer network to broadcast changes to the block chain or transactions. Sharing and loading these is done with the CLI.

## Getting started with the CLI

You can get list of all the commands by typing ``potaskacoin help``  

### Creating a wallet
To create a new wallet (public/private key pair) type ``potaskacoin init``  
After that you should have a ``config.json`` in the same folder that you extracted the executable to. This file holds your private and public key. You can share the public key to other people but you should never share your private key with anyone. Everybody who has your private key will be able to make transactions in your name.

###
At this point you should probably find the base64 of the most recent blockchain and load it using ``potaskacoin load chain <base64>``. The blockchain contains all the data of the transactions. You should probably also load the latest pending transactions with ``potaskacoin load transactions <base64>``
